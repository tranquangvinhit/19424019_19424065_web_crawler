
# khai báo thư viện
from numpy.lib.function_base import append
import requests
import os
from requests.api import get
import simplejson as json
from requests_download import download
from pymongo import MongoClient

# kết nối database
client = MongoClient(
    "mongodb+srv://dev:dev123456@cluster0.remdr.mongodb.net/demo?retryWrites=true&w=majority")
db = client["demo"]
collection = db["fpt"]
collection1 = db["hoangha"]

# THU THẬP DỮ LIỆU TỪ WEBSITE: https://fptshop.com.vn/
def get_product_fpt():
    # Tất cả thông tin lưu trữ trong đối tượng response
    # Tạo yêu cầu GET
    # Request gửi một yêu cầu HTTP
    # Truyền URL vào get
    # url này mình có thể đổi cateId
    response = requests.get(
        'https://fptshop.com.vn/apiFPTShop/Advertising/GetBannerByBrand?adsPosition=86&rootId=1&cateId=3')

    # Nội dung cung cấp cho bạn quyền truy cập vào các byte thô của requests
    # Nhưng muốn đổi chúng sang chuổi bằng cách mã hóa kí tự
    # response.text sẽ chuyển đổi
    data = response.text

    # json sử dụng để tải từ chuổic
    # json.loads: chấp nhận đối tượng tệp, phân tích cú pháp dữ liệu json, nhận vào và trả nội dung của tệp
    jsondata = json.loads(data)

    # Xem dữ liệu trả sao, rồi dẫn tới datas mình cần lấy
    dataList = jsondata['datas']['lst']
    #thêm vào database
    collection.insert_many(dataList)
    for row in dataList:
        pictureUrl = row['pictureUrl']
    # tải ảnh về thư mục img
        filename = os.path.basename(
            pictureUrl)
        folder = os.getcwd() + "/imgFPT/"
        download('https://images.fpt.shop/unsafe/fit-in/214x214/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/' +
                 pictureUrl, folder + filename)


 # THU THẬP DỮ WEBSITE https://hoanghamobile.com/
def get_product_hh():
   # url này có thể đổi size
    response = requests.get(
        'https://recommendation.api.useinsider.com/10005327/vi_VN/mixed?strategy=[%22ub%22,%22ub%22,%22ub%22,%22ub%22,%22ub%22,%22ub%22,%22ub%22,%22mvop%22,%22mvop%22,%22mvop%22,%22mvop%22,%22mvop%22,%22mvop%22,%22mvop%22,%22mvop%22]&userId=163090188755581ba2ea399.612a85e1&categoryList=[%22%C4%90i%E1%BB%87n%20tho%E1%BA%A1i%22]&details=true&size=15&currency=VND&productIds=&shuffle=true&')

    data = response.text
    jsondata = json.loads(data)
    dataList = jsondata['data']
    collection1.insert_many(dataList)
    for row in dataList:
        image_url = row['image_url']
        filename = os.path.basename(
            image_url)
        folder = os.getcwd() + "/imgHoangHa/"
        download(image_url, folder + filename)
get_product_fpt()
get_product_hh()
